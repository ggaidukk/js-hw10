let tabs = document.getElementsByClassName("tabs");

let tabsTitle = document.getElementsByClassName("tabs-title");
let text = document.getElementsByClassName("text");
let tabsContent = document.getElementsByClassName("tabs-content");
for (let i = 0; i < tabsTitle.length; i++) {
  tabsTitle[i].addEventListener("click", function () {
    let active = document.getElementsByClassName("active");
    active[0].className = active[0].className.replace(" active", "");

    this.className += " active";
  });
}

for (let i = 0; i < text.length; i++) {
  tabsTitle[i].addEventListener("click", function () {
    let active = document.getElementsByClassName("active-text");
    active[0].className = active[0].className.replace(" active-text", "");

    text[i].className += " active-text";
  });
}

